<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\Blog\BlogDtoCollection;
use App\DataTransferObjects\Category\CategoryDTO;
use App\DataTransferObjects\Category\CategoryDtoCollection;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\SearchCategoryRequest;
use App\Http\Responses\ApiResponse;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    private CategoryService $categoryService;

    /**
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return ApiResponse
     */
    public function index(): ApiResponse
    {
        return new ApiResponse(new CategoryDtoCollection(Category::all()->all()));
    }

    /**
     * @param Category $category
     * @return ApiResponse
     */
    public function show(Category $category): ApiResponse
    {
        return new ApiResponse(CategoryDTO::fromModel($category));
    }

    /**
     * @param CreateCategoryRequest $request
     * @return ApiResponse
     */
    public function store(CreateCategoryRequest $request): ApiResponse
    {
        $category = Category::create($request->validated());
        return new ApiResponse(CategoryDTO::fromModel($category));
    }

    /**
     * @param Category $category
     * @return ApiResponse
     */
    public function destroy(Category $category): ApiResponse
    {
        $category->delete();
        return new ApiResponse(CategoryDTO::fromModel($category));
    }

    /**
     * @param SearchCategoryRequest $request
     * @return ApiResponse
     */
    public function search(SearchCategoryRequest $request): ApiResponse
    {
        $categoryWithChildren = Category::where('name', $request->get("category_name"))->get();
        $blogs = $this->categoryService->getBlogsOfCategoryWithSubcategories($categoryWithChildren);
        return new ApiResponse(new BlogDtoCollection($blogs));
    }

}
