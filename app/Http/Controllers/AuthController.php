<?php

namespace App\Http\Controllers;

use App\Exceptions\LoginException;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Responses\ApiResponse;
use App\Models\User;
use App\Services\ImagableService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    /**
     * @var ImagableService
     */
    protected ImagableService $imagableService;

    /**
     * @param ImagableService $imagableService
     */
    public function __construct(ImagableService $imagableService)
    {
        $this->imagableService = $imagableService;
    }

    /**
     * @param RegisterRequest $request
     * @return ApiResponse
     */
    public function register(RegisterRequest $request): ApiResponse
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        if ($imageBase64 = $request->get('photo')) {
            $this->imagableService->handleImage($user, $imageBase64);
        }

        $role = Role::findByName('user');
        $user->assignRole($role->id);

        return new ApiResponse($user, 200);
    }

    /**
     * @param Request $request
     * @return ApiResponse
     * @throws LoginException
     */
    public function login(LoginRequest $request): ApiResponse
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            throw new LoginException();
        }

        return new ApiResponse(['token' => $token]);
    }

    /**
     * @return ApiResponse
     */
    public function logout(User $user): ApiResponse
    {
        auth()->logout();
        return new ApiResponse([], 200);
    }

}
