<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\Blog\BlogDTO;
use App\DataTransferObjects\Blog\BlogDtoCollection;
use App\Http\Requests\CreateBlogRequest;
use App\Http\Requests\DeleteBlogRequest;
use App\Http\Requests\PostCommentRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Http\Responses\ApiResponse;
use App\Models\Blog;
use App\Models\MadeReaction;
use App\Models\Media;
use App\Models\Reaction;
use App\Services\BlogService;
use App\Services\Files\Factories\FileServiceFactory;
use App\Services\ImagableService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * @var ImagableService
     */
    protected ImagableService $imagableService;

    /**
     * @param ImagableService $imagableService
     */
    public function __construct(ImagableService $imagableService)
    {
        $this->imagableService = $imagableService;
    }

    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function index(Request $request): ApiResponse
    {
        return new ApiResponse(new BlogDtoCollection(Blog::all()), 200);
    }

    /**
     * @param Blog $blog
     * @return ApiResponse
     */
    public function show(Blog $blog): ApiResponse
    {
        return new ApiResponse(BlogDTO::fromModel($blog));
    }

    /**
     * @param CreateBlogRequest $request
     * @return ApiResponse
     */
    public function store(CreateBlogRequest $request): ApiResponse
    {
        $data = $request->validated();
        $data["user_id"] = Auth::user()->id;
        $blog = Blog::create($data);
        if ($imageBase64 = $request->get('photo')) {
            $this->imagableService->handleImage($blog, $imageBase64);
        }
        return new ApiResponse(BlogDTO::fromModel($blog));
    }

    /**
     * @param Blog $blog
     * @param Request $request
     * @return ApiResponse
     */
    public function update(Blog $blog, UpdateBlogRequest $request): ApiResponse
    {
        $blog->update($request->all());
        if ($imageBase64 = $request->get('photo')) {
            $this->imagableService->handleImage($blog, $imageBase64);
        }
        return new ApiResponse(BlogDTO::fromModel($blog));
    }

    /**
     * @param Blog $blog
     * @return ApiResponse
     */
    public function destroy(Blog $blog, DeleteBlogRequest $request): ApiResponse
    {
        if ($blog->photo_url){
            $this->imagableService->handleDelete($blog);
        }
        $blog->delete();
        return new ApiResponse(BlogDTO::fromModel($blog));
    }

    /**
     * @param PostCommentRequest $request
     * @param Blog $blog
     * @return ApiResponse
     */
    public function comment(PostCommentRequest $request, Blog $blog): ApiResponse
    {
        $comment = $request->get("comment");
        $user = Auth::user();
        $user->comments()->attach($blog, ["comment" => $comment]);

        return new ApiResponse(["comment" => $comment, "blog_id" => $blog->id]);
    }

    public function react(Blog $blog, Reaction $reaction): ApiResponse
    {
        $user = Auth::user();
        if ($madeReaction = $user->getReactionOnBlog($blog->id)) {
            $madeReaction->reaction_id = $reaction->id;
            $madeReaction->save();
        }
        else {
            $blog->madeReactions()->create(["reaction_id" => $reaction->id, "user_id" => $user->id, 'blog_id' => $blog->id]);
        }

        return new ApiResponse(["reaction" => $reaction, "user" => $user, "blog" => BlogDTO::fromModel($blog)]);
    }

}
