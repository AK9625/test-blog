<?php

namespace App\Exceptions;

use Exception;

class LoginException extends Exception
{
    protected $message = 'Email or Password is incorrect';
}
