<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'photo_url',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function media(): MorphOne
    {
        return $this->morphOne(Media::class, 'mediaholder');
    }

    /**
     * @return BelongsToMany
     */
    public function comments(): BelongsToMany
    {
        return $this->belongsToMany(Blog::class, 'comments', 'blog_id', 'user_id')
            ->withPivot("comment");
    }

    /**
     * @return HasMany
     */
    public function madeReactions(): HasMany
    {
        return $this->hasMany(MadeReaction::class, 'user_id');
    }

    /**
     * @param int $blogId
     * @return mixed
     */
    public function getReactionOnBlog(int $blogId)
    {
        return $this->madeReactions()->madeReactionToBlog($blogId)->with('reaction')->first();
    }
}
