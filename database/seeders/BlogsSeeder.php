<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();
        $categoriesIterator = $categories->getIterator();
        $blogs = Blog::factory(8)->create()->each(function ($blog) use ($categoriesIterator) {
            $blog->categories()->attach($categoriesIterator->current());
            $categoriesIterator->next();
            $blog->comments()->attach(User::all()->random(), ['comment' => 'nice blog']);
            $blog->media()->create(["media_path" => substr($blog->photo_url, -15)]);
            $blog->save();
        });
    }
}
