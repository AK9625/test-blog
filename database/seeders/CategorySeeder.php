<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ["name" => "IT"],
            ["name" => "Web Development", "parent_id" => 1],
            ["name" => "Backend Development", "parent_id" => 2],
            ["name" => "Frontend Development", "parent_id" => 2],
            ["name" => "Cooking"],
            ["name" => "Salads", "parent_id" => 5],
            ["name" => "Avocado Salad", "parent_id" => 6],
            ["name" => "Summer Salad", "parent_id" => 6],
        ];

        foreach ($categories as $category){
            Category::create($category);
        }
    }
}
